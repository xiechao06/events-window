import EventsWindow from "./index";

test("empty", () => {
  let ew = new EventsWindow(100, 10);
  expect(Array.from(ew.events())).toStrictEqual(new Array(10).fill(null));
  expect(Array.from(ew.eventsReversed())).toStrictEqual(
    new Array(10).fill(null)
  );
});

describe("discarded", () => {
  let ew = new EventsWindow(100, 10);
  ew.add(100, Date.now() - 100);
  expect(Array.from(ew.events())).toStrictEqual(new Array(10).fill(null));
  expect(Array.from(ew.eventsReversed())).toStrictEqual(
    new Array(10).fill(null)
  );
  ew.add(100, Date.now() - 101);
  expect(Array.from(ew.events())).toEqual(new Array(10).fill(null));
  expect(Array.from(ew.eventsReversed())).toStrictEqual(
    new Array(10).fill(null)
  );
});

test("throws", () => {
  expect(() => {
    new EventsWindow(100, 9);
  }).toThrow(/exact division/);
});

describe("default", () => {
  let ew = new EventsWindow(5000, 1000);
  let now = 5000;
  ew.add(1, now - 4000, now);
  ew.add(2, now - 3000, now);
  ew.add(3, now - 2000, now);
  ew.add(4, now - 1000, now);
  ew.add(5, now, now);

  it("remember all", () => {
    expect(Array.from(ew.events(now))).toEqual([
      [1, now - 4000],
      [2, now - 3000],
      [3, now - 2000],
      [4, now - 1000],
      [5, now]
    ]);
    expect(Array.from(ew.eventsReversed(now))).toEqual([
      [5, now],
      [4, now - 1000],
      [3, now - 2000],
      [2, now - 3000],
      [1, now - 4000]
    ]);
    expect(Array.from(ew.events(now, 2000))).toEqual([
      [3, now - 2000],
      [4, now - 1000],
      [5, now]
    ]);
    expect(Array.from(ew.eventsReversed(now, 2000))).toEqual([
      [5, now],
      [4, now - 1000],
      [3, now - 2000]
    ]);
  });
  it("forget 1", () => {
    expect(Array.from(ew.events(now + 1000, 2000))).toEqual([
      [4, now - 1000],
      [5, now],
      null
    ]);
  });
  it("forget 1, 2", () => {
    expect(Array.from(ew.events(now + 2000))).toEqual([
      // forget 1, 2
      [3, now - 2000],
      [4, now - 1000],
      [5, now],
      null,
      null
    ]);
  });
  it("forget 1, 2, 3, 4", () => {
    expect(Array.from(ew.events(now + 4000))).toEqual([
      [5, now],
      null,
      null,
      null,
      null
    ]);
  });
  it("forget all", () => {
    expect(Array.from(ew.events(now + 5000))).toEqual(new Array(5).fill(null));
  });
});

test("discard events outside window", () => {
  const ew = new EventsWindow(50, 10);
  const now = Date.now();
  expect(ew.add("foo", now - 51, now)).toBe(false);
});
