# events-window

[![npm](https://img.shields.io/npm/v/events-window.svg?style=flat-square)](https://www.npmjs.org/package/events-window)
[![The MIT License](https://img.shields.io/badge/license-MIT-orange.svg?style=flat-square)](http://opensource.org/licenses/MIT)
[![Type definitions](https://img.shields.io/npm/types/typescript.svg)](https://www.typescriptlang.org/)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)

[Documentation](https://xiechao06.gitlab.io/events-window).

A data structure to store and retrieve events, it only store events
that are happened for a short while.

## Quick start

```javascript
import EventsWindow from 'events-window';

const ew = new EventsWindow();

ew.add('good message', Date.now());
ew.add('bad message', Date.now());

for (const [v, t] of ew.events(Date.now() - 2000)) {
    console.log(`receive event ${v} at ${t}`);
}
```

## Installation

```bash
npm i events-window;
```

## Development

```bash
# test
npm run test:watch
# build
npm run build
```
