import debug from "debug";

class EventsWindow {
  private size: number;
  private granularity: number;
  private bufs: Int32Array;
  private static debug: debug.Debugger = debug("events-window");
  /**
   * @param  {number} size the size of the window in milliseconds, this means
   *  only events happened in recent `size` milliseconds will be available.
   * @param  {number} granularity events happened in `granularity` milliseconds
   *  will be dropped.
   */
  constructor(size: number, granularity: number) {
    if (size % granularity !== 0) {
      throw new Error("granularity should be the exact division of size");
    }
    this.size = size;
    this.granularity = granularity;
    this.bufs = new Int32Array(this.cellNum * 2);
    this.clear();
  }

  private get cellNum() {
    return this.size / this.granularity;
  }

  /**
   * return events in window happened before `beforeTo` and after `beforeTo` - `size`
   * @param beforeTo where to begin, in milliseconds, if not specified use Date.now()
   * @param size the search should be stopped here, if not specified, return all
   *  the data
   */
  events(beforeTo?: number, size?: number) {
    if (beforeTo === void 0) {
      beforeTo = Date.now();
    }
    let ew = this;
    return {
      [Symbol.iterator]() {
        return (function*() {
          let lastCell = Math.floor(beforeTo! / ew.granularity);
          let startCell =
            size === void 0
              ? lastCell - ew.cellNum + 1
              : Math.floor((beforeTo! - size) / ew.granularity);
          while (startCell <= lastCell) {
            let idx = (startCell + ew.cellNum) % ew.cellNum;
            let evt = [ew.bufs[idx << 1], ew.bufs[(idx << 1) + 1]];
            // cell may contains stale data
            if (evt[1] === -1 || beforeTo! - evt[1] >= ew.size) {
              yield null;
            } else {
              yield evt;
            }
            ++startCell;
          }
        })();
      }
    };
  }

  /**
   * return events in window REVERSELY happened before `beforeTo` and after `beforeTo` - `size`
   * @param beforeTo where to begin, in milliseconds, if not specified use Date.now()
   * @param size the search should be stopped here, if not specified, return all
   *  the data
   */
  eventsReversed(beforeTo?: number, size?: number) {
    if (beforeTo === void 0) {
      beforeTo = Date.now();
    }
    let ew = this;
    return {
      [Symbol.iterator]() {
        return (function*() {
          let lastCell = Math.floor(beforeTo! / ew.granularity);
          let startCell =
            size === void 0
              ? lastCell - ew.cellNum + 1
              : Math.floor((beforeTo! - size) / ew.granularity);
          while (startCell <= lastCell) {
            let idx = (lastCell + ew.cellNum) % ew.cellNum;
            let evt = [ew.bufs[idx << 1], ew.bufs[(idx << 1) + 1]];
            // cell may contains stale data
            if (evt[1] === -1 || beforeTo! - evt[1] >= ew.size) {
              yield null;
            } else {
              yield evt;
            }
            --lastCell;
          }
        })();
      }
    };
  }
  /**
   * clear the events
   */
  clear() {
    for (let i = 0; i < this.bufs.length; ++i) {
      this.bufs[i] = -1;
    }
  }

  /**
   * add event
   * @param  {any} v the value of the event
   * @param  {number} t the timestamp of the event, if not specified, use Date.now()
   * @param  {number} now? if you set now, the events happened before `now` - `events window's size`
   *  will be discarded
   * @return return true if succeed, otherwise false
   */
  add(v: any, t: number, now?: number) {
    // discards events outside window
    if (now !== void 0 && now - t > this.size) {
      EventsWindow.debug(
        [
          `you can't add event happend at ${t}ms`,
          `it is at least ${this.size}ms before now(${now})`
        ].join(", ")
      );
      return false;
    }
    let idx = Math.floor((t % this.size) / this.granularity);
    this.bufs[idx << 1] = v;
    this.bufs[(idx << 1) + 1] = t;
    return true;
  }
}

export default EventsWindow;
